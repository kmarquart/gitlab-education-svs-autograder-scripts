from verifiers import *

# A. Sign in to the GitLab instance
# Not checkable.

# B. Create a new Node JS Express project with Auto DevOps
group_name: str = 'Group1'
project_name: str = 'CI Test'
project_path: str = f'{group_name}/{project_name}'
branch_name: str = 'main'
file_path: str = '.gitlab-ci.yml'
file_contents_1: str = 'include:'
file_contents_2: str = '- template: Security/SAST.gitlab-ci.yml'

if not file_contains_texts_remote(project_path, file_path, branch_name, [file_contents_1, file_contents_2]):
    fail_challenge("Failed in section B. "
                   f"On the '{branch_name}' branch "
                   f"of the '{project_name}' project "
                   f"in the '{group_name}' group, "
                   f"did you make a commit that updates the '{file_path}' file "
                   "to include the code to enable SAST?")

# C. Add "main.go" and review SAST scanning results

file_path: str = 'main.go'

if not file_exists_remote(project_path, file_path, branch_name):
    fail_challenge("Failed in section C. "
                   f"On the '{branch_name}' branch "
                   f"of the '{project_name}' project "
                   f"in the '{group_name}' group, "
                   f"did you make a commit that adds a '{file_path}' file?")

file_contents: str = 'http.ListenAndServe(":8080", nil)'

if not file_contains_text_remote(project_path, file_path, branch_name, file_contents):
    fail_challenge("Failed in section C. "
                   f"On the '{branch_name}' branch "
                   f"of the '{project_name}' project "
                   f"in the '{group_name}' group, "
                   "did you make a commit that adds the contents from the Go source code snippet "
                   f"to the '{file_path}' file?")

# D. Finish
# Not checkable.
