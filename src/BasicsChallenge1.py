from verifiers import *


# A. Sign in to the GitLab Instance
# Not checkable.

# B. Create a GitLab Group and Project

group_name: str = 'Training Group'

if not group_exists(group_name):
    fail_challenge("Failed in section B. "
                   f"Did you make a group called '{group_name}'? " + CASE_WARNING)

project_name: str = 'Top Level Project'
project_path: str = f'{group_name}/{project_name}'

if not project_exists_remote(project_path):
    fail_challenge("Failed in section B. "
                   f"Did you make a project called '{project_name}' "
                   f"in the '{group_name}' group? " + CASE_WARNING)

# C. Create an Issue

issue_title: str = 'my first issue'
if not issue_exists(project_path, issue_title):
    fail_challenge("Failed in section C. "
                   f"Did you make an issue called '{issue_title}' "
                   f"in the project '{project_name}' "
                   f"in the group '{group_name}'?  + CASE_WARNING")

# D. Create Custom Labels

label: str
for label in ['Opened', 'Completed', 'Needs documentation']:
    if not project_label_exists(project_path, label):
        fail_challenge("Failed in section D. "
                       f"Did you make a label called '{label}' "
                       f"in the '{project_name}' project "
                       f"in the '{group_name}' group? " + CASE_WARNING)

# E. Use a Quick Action

hours_spent: int = 1

if not time_spent(project_path, issue_title, hours_spent):
    fail_challenge("Failed in section E. "
                   f"Did you mark {hours_spent} hour spent "
                   f"on the '{issue_title}' issue "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

# F. Assign Labels to an Issue

for label in ['Opened', 'Needs documentation']:
    if not issue_has_label(project_path, issue_title, label):
        fail_challenge("Failed in section F. "
                       f"Did you assign the '{label}' label "
                       f"to the '{issue_title}' issue "
                       f"in the '{project_name}' project "
                       f"in the '{group_name}' group?")
