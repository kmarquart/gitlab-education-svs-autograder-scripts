from verifiers import *

# A. Sign in to the GitLab Instance
# Not checkable.

# B. Create a new project and enable code quality scanning

group_name: str = 'Group1'
project_name: str = 'Code Quality Demo'
project_path: str = f"{group_name}/{project_name}"

if not project_exists_remote(project_path):
    fail_challenge("Failed in section B. "
                   f"Did you create a project called '{project_name}' "
                   f"in the '{group_name}' group?")

variable_name: str = 'CODE_QUALITY_DISABLED'
variable_value: str = ' '
if not project_level_var_exists(project_path, variable_name, variable_value):
    fail_challenge("Failed in section B. "
                   f"Did you create a project-level variable called '{variable_name}' "
                   f"with the value '{variable_value}' "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

# C. Add a Python file with code quality problems

file_path: str = 'HelloWorld.py'
branch_name: str = 'main'
text: str = """def hello_world(a, b, c, d, e, f, g):
    print("Hello world")
    # TODO: improve this function"""

if not file_exists_remote(project_path, file_path, branch_name):
    fail_challenge("Failed in section C. "
                   f"Did you make a new file '{file_path}' "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section C. "
                   "Did you paste the Python code "
                   f"into the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? "
                   f"Remember to check indentation.")

# D. Enable code quality scanning in ".gitlab-ci.yml"

file_path: str = '.gitlab-ci.yml'

if not file_exists_remote(project_path, file_path, branch_name):
    fail_challenge("Failed in section D. "
                   f"Did you make a new file '{file_path}' "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

text: str = """stages:
  - test"""

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section D. "
                   "Did you create a 'test' stage "
                   f"in the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? "
                   f"Remember to check indentation to match the example in the instructions.")

text: str = '''test-job:
  stage: test
  script:
    - echo "Pipeline needs at least one job"'''

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section D. "
                   "Did you define a job called 'test-job' "
                   f"in the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? "
                   f"Remember to copy the job definition exactly from the instructions.")

text: str = """include:
  - template: Code-Quality.gitlab-ci.yml"""

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section D. "
                   "Did you enable Code Quality scanning "
                   f"in the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? "
                   f"Remember to copy the code exactly from the instructions, and check indentation.")

text: str = 'TODO: should we refactor this file?'

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section D. "
                   "Did you include a 'TODO' comment "
                   f"in the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

# E. View code quality scan results
# Nothing worth checking here.

# F. Make a branch

branch_name: str = 'fix-code-quality-problems'

if not branch_exists_remote(project_path, branch_name):
    fail_challenge("Failed in section F. "
                   f"Did you create a branch called '{branch_name}' "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

merge_request_name: str = 'Draft: Fix code quality problems'

if not project_has_merge_request(project_path, merge_request_name):
    fail_challenge("Failed in section F. "
                   f"Did you create a merge request called '{merge_request_name}' "
                   f"with '{branch_name}' as a source branch "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

# G. Fix issues on the branch

file_path: str = 'HelloWorld.py'
text: str = 'def hello_world(a):'

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section G. "
                   f"Did you paste in the fix to line 1 "
                   f"in the '{file_path}' file "
                   f"on the '{branch_name}' branch "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")


text: str = 'TODO: improve this function'

if file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section G. "
                   f"Did you remove the TODO item "
                   f"in the '{file_path}' file "
                   f"on the '{branch_name}' branch "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

# H. Compare the code quality of the "fix-code-quality-problems" and "main" branches
# Not checkable.

# I. Finish
# Not checkable.
