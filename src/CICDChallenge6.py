from verifiers import *

# A. Sign in to the GitLab Instance
# Not checkable.

# B. Add a Job Policy Pattern

group_name: str = 'Group1'
project_name: str = 'CICD Demo'
project_path: str = f"{group_name}/{project_name}"
file_path: str = '.gitlab-ci.yml'
branch_name: str = 'main'

stages_text: str = """stages:
  - test
  - build
  - review
  - deploy"""

if not file_contains_text_remote(project_path, file_path, branch_name, stages_text):
    fail_challenge("Failed in section B. "
                   "Did you add 'review' and 'deploy' stages, in that order, "
                   f"to the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? Remember to check indentation.")

job_policy_pattern_text: str = """deploy review:
  stage: review
  # only: 
  #   - branches
  # except: 
  #   - master
  script:
    - echo "Do your average deploy here"
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: '$CI_COMMIT_TAG'
      when: never
    - when: always
  environment:
    name: review/$CI_COMMIT_REF_NAME

deploy release:
  stage: deploy
  # only:
  #   - tags
  # except: 
  #   - master
  script:
    - echo "Deploy to a production environment"
  rules:
    - if: '$CI_COMMIT_REF_NAME != "master" && $CI_COMMIT_TAG'
      when: manual
  environment:
    name: production

deploy staging:
  stage: deploy
  # only:
  #   - master
  script:
    - echo "Deploy to a staging environment"
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: always
    - when: never
  environment:
    name: staging"""

if not file_contains_text_remote(project_path, file_path, branch_name, job_policy_pattern_text):
    fail_challenge("Failed in section B. "
                   "Did you paste content from the 'ci-structure' snippet "
                   f"into the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? Remember to check indentation.")

# C. Finish
# Not checkable.
