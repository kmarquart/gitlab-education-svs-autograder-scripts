from verifiers import *

# A. Sign in to the GitLab instance
# Not checkable.

# B. Verify that Git is installed locally
# Not checkable, since Git is installed by default on the master VM that's
# cloned for students to use in the training environment.

# C. Generate an SSH key
# Not checkable, since there's already a `~/.ssh/id_rsa` file on the master VM
# that's cloned for students to use in the training environment.

# D. Add an SSH key to your Gitlab profile

if not profile_has_ssh_key():
    fail_challenge("Failed in section D. "
                   "Could not find an SSH key in your user profile. Did you upload one?")

# E. Clone a project repository to your local machine
# In this case, they'll be cloning to their Instruqt terminal environment.

project_parent_dirs: str = '/root/training'
project_name: str = 'Top Level Project'
project_path_local: str = f'{project_parent_dirs}/{project_name}'
project_path_on_filesystem = format_path_for_filesystem(project_path_local)

if not project_exists_local(project_path_on_filesystem):
    fail_challenge("Failed in section E. "
                   f"Did you clone the '{project_name}' project "
                   f"into the '{project_parent_dirs}' directory "
                   "in your training environment's terminal? " + CASE_WARNING)

# F. Work on a Branch

branch_name: str = 'temporary_branch'

if not branch_exists_local(project_path_on_filesystem, branch_name):
    fail_challenge("Failed in section F. "
                   f"Did you make a branch called '{branch_name}' "
                   "in the local Git repository? " + CASE_WARNING)

# G. Edit a File

os.system(f'git checkout {branch_name}')

file_name: str = 'README.md'
file_path: str = f'{project_path_on_filesystem}/{file_name}'
file_contents: str = 'a line added to temporary_branch locally'

if not file_contains_text_local(file_path, file_contents):
    fail_challenge("Failed in section G. "
                   f"Did you add the line '{file_contents}' "
                   f"to the file '{file_name}' "
                   f"on the '{branch_name}' Git branch?" + CASE_WARNING)

# H. Add README.md to the Git staging area
# Not checkable because a later step will move it out of the staging area.

# I. Commit the changes to README.md
# Already checked in step G.

# J. Push your changes to temporary_branch on the remote Git repository on the GitLab instance

group_name: str = 'Training Group'
project_path_remote = f'{group_name}/{project_name}'

if not branch_exists_remote(project_path_remote, branch_name):
    fail_challenge("Failed in section J. "
                   f"Could not find a branch called '{branch_name}' "
                   f"in the project '{project_name}' "
                   f"in the group '{group_name}' "
                   "on the GitLab instance. "
                   "Did you push the local repository's commits to the remote repository?")

# K. Edit, commit, and push the file again

file_contents: str = 'another line in README.md'

if not file_contains_text_local(file_name, file_contents):
    fail_challenge("Failed in section K. "
                   f"Did you add the line '{file_contents}' "
                   f"to the file '{file_name}' "
                   f"on the '{branch_name}' Git branch?" + CASE_WARNING)


if not file_contains_text_remote(project_path_remote, file_name, branch_name, file_contents):
    fail_challenge("Failed in section K. "
                   f"Did you push the commit that contains your second edit of '{file_name}' "
                   "to the remote repository?")

# L. Simulate a change on the remote temporary_branch

file_contents: str = 'a fourth line added to the remote copy of temporary_branch'

if not file_contains_text_remote(project_path_remote, file_name, branch_name, file_contents):
    fail_challenge("Failed in section L. "
                   f"Did you add the line '{file_contents}' "
                   f"to the file '{file_name}' "
                   f"on the '{branch_name}' branch "
                   "in the remote repository?" + CASE_WARNING)

# M. Get metadata about changes to the remote temporary_branch
# Not checkable.

# N. Pull from the remote (upstream) repository

os.system(f'git checkout {branch_name}')

if not file_contains_text_local(file_name, file_contents):
    fail_challenge("Failed in section N. "
                   "Did you pull changes from the remote repository to the local repository?")

# O. Merge changes from temporary_branch into the main branch

source_branch_name: str = 'temporary_branch'
target_branch_name: str = 'main'
file_contents: str = 'a fourth line added to the remote copy of temporary_branch'

os.system(f'git checkout {target_branch_name}')
if not file_contains_text_local(file_name, file_contents):
    fail_challenge("Failed in section O. "
                   f"Did you merge '{source_branch_name}' into '{target_branch_name}' in the local repository?")

# P. Update the remote repository
if not file_contains_text_remote(project_path_remote, file_name, target_branch_name, file_contents):
    fail_challenge(
        "Failed in section P. "
        f"Did you push changes on the local repository's '{target_branch_name}' branch "
        "to the remote repository?")
