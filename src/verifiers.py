import base64
import os
from helpers import *


def project_exists_local(project_path: str) -> bool:
    return os.path.isdir(f'{project_path}/.git')


def project_exists_remote(project_path: str) -> bool:
    """Does a project exist on the GitLab instance at a given path?

    :param project_path: the project to look for
    :return: whether a project exists at that path
    """

    formatted_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_path}'
    response: Response = call_api(url)
    return response.status_code == 200


def group_exists(group_path: str) -> bool:
    """Does a group exist on the GitLab instance at a given path?

    :param group_path: the group to look for
    :return: whether a group exists at that path
    """

    formatted_path: str = format_path_for_url(group_path)
    url: str = f'{BASE_API_URL}/groups/{formatted_path}'
    response: Response = call_api(url)
    return response.status_code == 200


def issue_exists(project_path: str, expected_issue_title: str) -> bool:
    """Does the project have a particular issue?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param expected_issue_title: issue title in human-readable format
    :return: whether the project has an issue with that title
    """

    formatted_project_path: str = format_path_for_url(project_path)
    urlencoded_issue_title: str = urlencode(expected_issue_title)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/issues?search={urlencoded_issue_title}'
    response: Response = call_api(url)
    # if it finds no issues, API returns code 200 with an empty list
    issues: list = json.loads(response.content)
    issue: dict
    # this API call returns issues with *similar* names to the one in our query, so iterate
    # through all issues returned and make sure one of them has the exact name we're looking for
    for issue in issues:
        actual_issue_title: str = issue['title']
        if actual_issue_title == expected_issue_title:
            return True
    return False


def issue_assigned(project_path: str, issue_title: str, expected_assignee: str) -> bool:
    """Is the issue assigned to a particular person?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param issue_title: issue title in human-readable format
    :param expected_assignee: the <b>username</b> of the person the issue might be assigned to
    :return: whether the issue is assigned to the person
    """

    formatted_project_path: str = format_path_for_url(project_path)
    urlencoded_issue_title: str = urlencode(issue_title)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/issues?search={urlencoded_issue_title}'
    response: Response = call_api(url)
    issues: list = json.loads(response.content)
    issue: dict
    for issue in issues:
        if issue['title'] == issue_title:
            actual_assignee: str = issue['assignee']['username']
            return actual_assignee == expected_assignee
    return False


def issue_has_label(project_path: str, issue_title: str, expected_label: str) -> bool:
    """Does the issue have a particular label assigned to it?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param issue_title: issue title in human-readable format
    :param expected_label: the label that might be assigned to this issue
    :return: whether the label is assigned to the issue
    """

    # get the issue
    formatted_project_path: str = format_path_for_url(project_path)
    urlencoded_issue_title: str = urlencode(issue_title)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/issues?search={urlencoded_issue_title}'
    response: Response = call_api(url)
    issues: list = json.loads(response.content)
    issue: dict
    for issue in issues:
        # the API call could return multiple issues that have titles *similar* to the issue title
        # we searched for, so go through them all and pick out the one with exactly the right title
        if issue['title'] == issue_title:
            actual_labels: list = issue['labels']
            actual_label: str
            for actual_label in actual_labels:
                if actual_label == expected_label:
                    return True
    return False


def project_label_exists(project_path: str, label: str) -> bool:
    """Does the project define a particular label?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param label: the label that might be defined by the project
    :return: whether the project defines that label
    """

    formatted_project_path: str = format_path_for_url(project_path)
    urlencoded_label: str = urlencode(label)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/labels/{urlencoded_label}'
    response: Response = call_api(url)
    return response.status_code == 200


def time_spent(project_path: str, issue_title: str, hours_spent_expected: float) -> bool:
    """Has an issue had a particular number of hours spent on it?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param issue_title: issue title in human-readable format
    :param hours_spent_expected: the number of hours
    :return: whether the issue has had the specific number of hours spent on it
    """

    # get the issue
    formatted_project_path: str = format_path_for_url(project_path)
    urlencoded_issue_title: str = urlencode(issue_title)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/issues?search={urlencoded_issue_title}'
    response: Response = call_api(url)
    issues: list = json.loads(response.content)
    issue: dict
    for issue in issues:
        # the API call could return multiple issues that have titles *similar* to the issue title
        # we searched for, so go through them all and pick out the one with exactly the right title
        if issue['title'] == issue_title:
            seconds_spent_actual: int = issue['time_stats']['total_time_spent']
            hours_spent_actual: float = seconds_spent_actual / (60.0 * 60.0)
            return hours_spent_expected == hours_spent_actual
    return False


def profile_has_ssh_key() -> bool:
    """Does the GitLab instance have any SSH keys defined in user profiles?

    This returns `True` if any users have SSH keys in their profiles, but since
    each student will have their own GitLab instance, in effect this is telling us
    if the student has added any SSH keys in their own profile.

    :return: whether any users of this GitLab instance have any SSH keys added to their profiles
    """

    url: str = f'{BASE_API_URL}/user/keys'
    response: Response = call_api(url)
    keys: list = json.loads(response.content)
    return len(keys) > 0


def branch_exists_local(project_path: str, branch_name: str) -> bool:
    """Does the local copy of a project have a particular branch?

    :param project_path: project path in human-readable format (e.g., "CodeDir/My Project")
    :param branch_name: the branch to look for
    :return: whether the project has the specified branch
    """

    project_path_on_filesystem = format_path_for_filesystem(project_path)
    os.chdir(project_path_on_filesystem)
    stream = os.popen('git branch')
    output: str = stream.read()
    return branch_name in output


def file_exists_remote(project_path: str, file_path: str, branch_name: str) -> bool:
    """Does a project on GitLab have a file in its repository?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param file_path: path to the file
    :param branch_name: branch to look for the file on
    :return: whether the project on GitLab has the specified file in the specified branch
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/repository/files/{file_path}?ref={branch_name}'
    response: Response = call_api(url)
    return response.status_code == 200


def file_contains_text_local(file_path: str, text: str) -> bool:
    """Does local Git repository have a file with a certain text string in its contents, in the current branch?

    :param file_path: path to the file
    :param text: the string to look for in the file's contents
    :return: whether the local file has the specified string in its contents
    """

    if not os.path.isfile(file_path):
        return False
    with open(file_path) as f:
        contents: str = f.read()
        return text in contents


def file_contains_text_remote(project_path: str, file_path: str, branch_name: str, expected_text: str) -> bool:
    """Does a project on GitLab have a file with a certain string in its contents, on the specified branch?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param file_path: path to the file
    :param branch_name: branch to look for the file in
    :param expected_text: the text to look for in the file
    :return: whether the project on GitLab has the specified file on the specified branch with the specified text
    """

    formatted_project_path: str = format_path_for_url(project_path)
    # formatted_file_path: str = format_path_for_url(file_path)
    encoded_file_path: str = urlencode(file_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/repository/files/{encoded_file_path}?ref={branch_name}'
    response: Response = call_api(url)
    file_info: dict = json.loads(response.content)
    base64_encoded_file_content: str = file_info['content']
    base64_encoded_file_content_bytes: bytes = base64_encoded_file_content.encode('ascii')
    file_content_bytes: bytes = base64.b64decode(base64_encoded_file_content_bytes)
    file_content: str = file_content_bytes.decode('ascii')
    return expected_text in file_content


def file_contains_texts_remote(project_path: str, file_path: str, branch_name: str, texts: list) -> bool:
    """Does a project on GitLab have a file with multiple strings in its contents, on the specified branch?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param file_path: path to the file
    :param branch_name: branch to look for the file in
    :param texts: a list of text strings to look for in the file
    :return: whether the project on GitLab has the specified file on the specified branch
             with ALL the specified strings
    """

    text: str
    for text in texts:
        if not file_contains_text_remote(project_path, file_path, branch_name, text):
            return False
    return True


def branch_exists_remote(project_path: str, expected_branch_name: str) -> bool:
    """Does a project on GitLab have a specific branch?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param expected_branch_name: the branch to look for
    :return: whether the project has the specified branch
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/repository/branches'
    response: Response = call_api(url)
    branches: list = json.loads(response.content)
    branch: dict
    for branch in branches:
        actual_branch_name: str = branch['name']
        if actual_branch_name == expected_branch_name:
            return True
    return False


def commit_exists_with_message_remote(project_path: str, branch_name: str, expected_commit_msg: str) -> bool:
    """Does a project on GitLab have a commit with a specific message on a specific branch?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param branch_name: the branch to look in
    :param expected_commit_msg: the entire commit message
    :return:
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/repository/commits?ref_name={branch_name}'
    response: Response = call_api(url)
    commits: list = json.loads(response.content)
    commit: dict
    for commit in commits:
        actual_commit_msg: str = commit['message']
        if actual_commit_msg == expected_commit_msg:
            return True
    return False


def project_has_visibility(project_path: str, expected_visibility: str) -> bool:
    """Does a project have the specified visibility level?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param expected_visibility: the visibility to look for
    :return: whether the project has the specified visibility
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}'
    response: Response = call_api(url)
    project_info: dict = json.loads(response.content)
    actual_visibility: str = project_info['visibility']
    return actual_visibility == expected_visibility


def project_has_merge_request(project_path: str, expected_merge_request_title: str) -> bool:
    """Does a project have a merge request with a specific title?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param expected_merge_request_title: the title to look for
    :return: whether the project has a merge request with the specified title
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/merge_requests'
    response: Response = call_api(url)
    merge_requests: list = json.loads(response.content)
    merge_request: dict
    for merge_request in merge_requests:
        actual_merge_request_title: str = merge_request['title']
        if actual_merge_request_title == expected_merge_request_title:
            return True
    return False


def merge_request_has_human_comment(project_path: str, merge_request_title: str) -> bool:
    """Does a specific merge request in a specific project have any human-created comments?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param merge_request_title: the title to look for
    :return: whether the merge request in the project has any human-created comments
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/merge_requests?scope=assigned_to_me'
    response: Response = call_api(url)
    merge_requests: list = json.loads(response.content)
    merge_request: dict
    for merge_request in merge_requests:
        if merge_request['title'] == merge_request_title:
            merge_request_iid: int = merge_request['iid']
            url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/merge_requests/' \
                       f'{merge_request_iid}/discussions'
            response: Response = call_api(url)

    # There are probably many system-generated comments, such as the comments that are created when
    # you assign an MR to yourself. Look through all comments to see if there are any that weren't
    # generated by the system.

    comments: list = json.loads(response.content)
    comment: dict
    for comment in comments:
        comment_notes: list = comment['notes']
        comment_note: dict = comment_notes[0]
        created_by_human: bool = not comment_note['system']
        if created_by_human:
            return True
    return False


def merge_request_merged(project_path: str, merge_request_title: str) -> bool:
    """Has a particular merge request been merged?

    :param project_path: project path in human-readable format (e.g., "Group1/My Project")
    :param merge_request_title: the title to look for
    :return: whether the merge request in the project has been merged
    """

    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/merge_requests?state=merged'
    response: Response = call_api(url)
    merge_requests: list = json.loads(response.content)
    merge_request: dict
    for merge_request in merge_requests:
        if merge_request['title'] == merge_request_title:
            return True
    return False


def group_level_var_exists(group_path: str, expected_var_name: str, expected_var_value: str) -> bool:
    formatted_group_path: str = format_path_for_url(group_path)
    url: str = f'{BASE_API_URL}/groups/{formatted_group_path}/variables'
    response: Response = call_api(url)
    group_level_vars: list = json.loads(response.content)
    group_level_var: dict
    for group_level_var in group_level_vars:
        actual_var_name: str = group_level_var['key']
        actual_var_value: str = group_level_var['value']
        if (actual_var_name == expected_var_name) and (actual_var_value == expected_var_value):
            return True
    return False


def project_level_var_exists(project_path: str, expected_var_name: str, expected_var_value: str) -> bool:
    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/variables'
    response: Response = call_api(url)
    project_level_vars: list = json.loads(response.content)
    project_level_var: dict
    for project_level_var in project_level_vars:
        actual_var_name = project_level_var['key']
        actual_var_value = project_level_var['value']
        if (actual_var_name == expected_var_name) and (actual_var_value == expected_var_value):
            return True
    return False


def artifact_exists(project_path: str):
    formatted_project_path: str = format_path_for_url(project_path)
    url: str = f'{BASE_API_URL}/projects/{formatted_project_path}/jobs'
    response: Response = call_api(url)
    jobs: list = json.loads(response.content)
    latest_job: dict = jobs[0]
    artifacts: list = latest_job['artifacts']
    return len(artifacts) > 0
