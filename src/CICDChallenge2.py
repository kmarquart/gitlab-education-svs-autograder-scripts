from verifiers import *

# A. Sign in to the GitLab Instance
# Not checkable.

# B. Is the gitlab-runner binary already installed on your computer?
# Not checkable.

# C. Install the gitlab-runner binary on your computer
# Not checkable.

# D. Create a project
group_name: str = 'Group1'
project_name: str = 'CICD Demo'
project_path: str = f"{group_name}/{project_name}"

if not project_exists_remote(project_path):
    fail_challenge("Failed in section C. "
                   f"Did you create a project called '{project_name}' "
                   f"in the '{group_name}' group?")

# E. Register a specific GitLab Runner dedicated to your project
# I think we need to take this step out, because it looks like a GitLab Runner is already installed on the
# Instruqt master VM.

# F. Add a `.gitlab-ci.yml` file
file_path: str = '.gitlab-ci.yml'
branch_name: str = 'main'
text: str = '''stages:
  - build
  - test

build1:
  stage: build
  script:
    - echo "Do your build here"

test1:
  stage: test
  script:
    - echo "Do a test here"
    - echo "For example run a test suite"'''

if not file_exists_remote(project_path, file_path, branch_name):
    fail_challenge("Failed in section F. "
                   f"Did you create a file called '{file_path}' "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section F. "
                   f"Did you add the right content to the '{file_path}' file? "
                   f"Did you double-check all whitespace so it matches the text in the instructions exactly?")

# G. View a pipeline’s status, stages, jobs, and gitlab-runner
# Not checkable.

# H. Finish
# Not checkable.
