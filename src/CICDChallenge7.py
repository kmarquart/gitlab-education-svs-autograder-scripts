from verifiers import *

# A. Sign in to the GitLab Instance
# Not checkable.

# B. Add artifacts to your pipeline

group_name: str = 'Group1'
project_name: str = 'CICD Demo'
project_path: str = f"{group_name}/{project_name}"
file_path: str = '.gitlab-ci.yml'
branch_name: str = 'main'

text: str = """build app:
  image: golang:latest
  stage: build
  script:
    - go build -o app main.go
  artifacts:
    paths:
      - app
    expire_in: 1 hour"""

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section B. "
                   "Did you paste content from the 'ci-artifacts' snippet "
                   f"into the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group? "
                   f"Remember to check indentation.")


# C. Add a "main.go" file

file_path: str = 'main.go'

if not file_exists_remote(project_path, file_path, branch_name):
    fail_challenge("Failed in section C. "
                   f"Did you add a new file '{file_path}' "
                   f"to the '{project_name}' project "
                   f"in the '{group_name}' group?")


text: str = """package main

import (
	"fmt"
	"net/http"
)

func helloworld() string {
	return "Hello World!!"
}

func healthcheck() string {
	return "Health OK!"
}

func livenesscheck() string {
	return "I am alive!!!"
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, helloworld())
	})

	http.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, healthcheck())
	})

	http.HandleFunc("/liveness", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, livenesscheck())
	})

	http.ListenAndServe(":8080", nil)
}"""

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section C. "
                   f"Did you follow the instructions about pasting code into "
                   f"into the '{file_path}' file "
                   f"to the '{project_name}' project "
                   f"in the '{group_name}' group?")

if not artifact_exists(project_path):
    fail_challenge("Failed in section C. "
                   f"Did you make sure the pipeline created an 'app' artifact?")


# D. Finish
# Not checkable.
