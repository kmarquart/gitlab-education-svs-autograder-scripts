from verifiers import *

# A. Sign in to the GitLab Instance
# Not checkable.

# B. Add custom variables

group_name: str = 'Group1'
project_name: str = 'CICD Demo'
project_path: str = f"{group_name}/{project_name}"
file_path: str = '.gitlab-ci.yml'
branch_name: str = 'main'
text: str = '''environment variables:
  stage: build
  script:
    - echo "Do a test here"
    - echo "Here are some default, global, & local variables..."
    - echo $CI_COMMIT_SHORT_SHA
    - echo $group_level_variable
    - echo $project_level_variable
    - echo $INLINE_GLOBAL_VARIABLE
    - echo $INLINE_LOCAL_VARIABLE'''

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section B. "
                   "Did you paste content from the 'ci-variables' snippet "
                   f"into the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")


text: str = """variables:
  INLINE_GLOBAL_VARIABLE: "I'm an inline variable set at the global level of the CI/CD configuration file"""

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section B. "
                   "Did you set the 'INLINE_GLOBAL_VARIABLE' variable to exactly the value specified in the "
                   "instructions, using a 'variables' keyword with global scope "
                   f"in the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

text: str = '''environment variables:
  stage: build
  script:
    - echo "Do a test here"
    - echo "Here are some default, global, & local variables..."
    - echo $CI_COMMIT_SHORT_SHA
    - echo $group_level_variable
    - echo $project_level_variable
    - echo $INLINE_GLOBAL_VARIABLE
    - echo $INLINE_LOCAL_VARIABLE
  variables:
    INLINE_LOCAL_VARIABLE: "I'm an inline variable set at the job level of the CI/CD configuration file"'''

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section B. "
                   "Did you set the 'INLINE_LOCAL_VARIABLE' variable to exactly the value specified in the "
                   "instructions, using a 'variables' keyword immediately under the 'stage' keyword "
                   "within the 'environment variables' job definition, "
                   f"in the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")


# C. Add group- and project-level variables

group_variable_name: str = 'group_level_variable'
group_variable_value: str = "I'm a variable set at the group level"

if not group_level_var_exists(group_name, group_variable_name, group_variable_value):
    fail_challenge("Failed in section C. "
                   f"Did you add a group-level variable called '{group_variable_name}' "
                   f"with the value '{group_variable_value}' "
                   f"to the '{group_name}' group?")


project_variable_name: str = 'project_level_variable'
project_variable_value: str = "I'm a variable set at the project level"

if not project_level_var_exists(project_path, project_variable_name, project_variable_value):
    fail_challenge("Failed in section C. "
                   f"Did you add a project-level variable called '{project_variable_name}' "
                   f"with the value '{project_variable_value}' "
                   f"to the '{project_name}' path "
                   f"in the '{group_name}' group?")

# D. Finish
# Not checkable.
