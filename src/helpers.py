from config import *
import json
import requests
from requests import Response
import sys
import urllib.parse


def call_api(url: str) -> Response:
    """Call a REST API endpoint.

    :rtype: object
    :param url: the full URL of the REST API call, including any parameters
    :return: response object
    """

    if ENABLE_DEBUG_INFO:
        print(f"sending request to URL: {url}")

    response = requests.get(url, headers=REQUEST_HEADERS)

    if ENABLE_DEBUG_INFO:
        print(f"response code: {response.status_code}")
        pretty_print_response(response)

    if response.status_code == 401:
        fail_challenge(f"could not authenticate with Personal Access Token: '{PERSONAL_ACCESS_TOKEN}'")
    return response


def urlencode(unencoded_string: str) -> str:
    """Convert a non-urlencoded string into a urlencoded String.

    For example: "foo/bar" becomes "foo%2Fbar"

    :param unencoded_string: the string in human-readable form
    :return: urlencoded version of the unencoded string
    """

    return urllib.parse.quote(unencoded_string, safe='')


def format_path_for_filesystem(project_path: str) -> str:
    """Convert a human-readable project path into a format used to store
    that project on a computer's filesystem.

    For example: "Group1/Top Level Project" becomes "group1/top-level-project"

    :param project_path: the project's path in human-readable form
    :return: the project's path in a form appropriate for navigating to that project on a local filesystem
    """
    lower_cased: str = project_path.lower()
    hyphenated: str = lower_cased.replace(' ', '-')
    return hyphenated


def format_path_for_url(path: str) -> str:
    """Convert human-readable group or project paths into the format
    used in GitLab URLs.

    For example: "Group 1/My Project" becomes "group-1%2fmy-project"
    :return: GitLab URL-compatible version of the group or project path
    """

    lower_cased: str = path.lower()
    hyphenated: str = lower_cased.replace(' ', '-')
    url_encoded: str = urlencode(hyphenated)
    return url_encoded


def pretty_print_response(response: Response) -> None:
    """Print the contents of the response from a GitLab REST API call

    :param response: returned from a GitLab REST API call
    """
    response_as_json: dict = json.loads(response.content)
    print(json.dumps(response_as_json, indent=2))


def fail_challenge(msg: str) -> None:
    """Tell Instruqt that the student did something wrong during the challenge,
    so it can mark the challenge as failed. Also, give the user feedback on what they did wrong.

    :param msg: text presented to the user giving feedback on what they did wrong.
    """
    print(f"FAIL: {msg}")
    sys.exit(1)
