from verifiers import *

# A. Sign in to the GitLab Instance
# Not checkable.

# B. Display Pipeline Environment Info
group_name: str = 'Group1'
project_name: str = 'CICD Demo'
project_path: str = f"{group_name}/{project_name}"
file_path: str = '.gitlab-ci.yml'
branch_name: str = 'main'
text: str = """environment echoes:
  stage: build
  script:
    - echo "Who am I running as..."
    - whoami
    - echo "Where am I..."
    - pwd
    - ls -al
    - echo "Here's what is available in our environment..."
    - env"""

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge("Failed in section B. "
                   "Did you paste content from the 'ci-environment' snippet "
                   f"into the '{file_path}' file "
                   f"in the '{project_name}' project "
                   f"in the '{group_name}' group?")

# There's no point checking for the pipeline run, since it would have triggered automatically
# when the student committed the above edit to `.gitlab-ci.yml`.

# C. Finish
# Not checkable.
