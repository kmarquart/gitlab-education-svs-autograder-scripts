# We want one set of constant values when these scripts are running
# on an actual Instruqt VM, and another set of constant values
# when they're running on a GitLab developer's machine.

# First, check what environment we're running in (Instruqt or somewhere else)
import socket
HOSTNAME_WHERE_SCRIPT_IS_RUNNING: str = socket.gethostname()
IN_INSTRUQT: bool = (HOSTNAME_WHERE_SCRIPT_IS_RUNNING == 'gitlab-base-ubuntu')

# Second, print the URL used by each REST API call if the script is not running
# in Instruqt. This helps with debugging.
ENABLE_DEBUG_INFO: bool = False if IN_INSTRUQT else True

# Third, set the URL to use for all REST API calls.
INSTRUQT_BASE_URL: str = 'http://127.0.0.1:8080/api/v4'
CHRIS_MAC_BASE_URL: str = 'http://192.168.0.129/api/v4'  # other developers need to change this
BASE_API_URL: str = INSTRUQT_BASE_URL if IN_INSTRUQT else CHRIS_MAC_BASE_URL

# Fourth, set the Personal Access Token (PAT) used to authenticate REST API calls
PAT_INSTRUQT: str = 'zQaGtz5vzSFz56AG3VxD'  # for `gitlab-student` user on master VM
PAT_CHRIS_MAC: str = 'xFn_vMaDtMjVp2Px57iJ'
PAT_CHRIS_LINUX: str = 'F9e6EesvFQF53wXYeqEt'
PERSONAL_ACCESS_TOKEN: str = PAT_INSTRUQT if IN_INSTRUQT else PAT_CHRIS_LINUX

REQUEST_HEADERS: dict = {
    'Accept': 'application/json',
    'PRIVATE-TOKEN': PERSONAL_ACCESS_TOKEN
}

CASE_WARNING: str = 'All text is case-sensitive.'  # Used in some failure messages.