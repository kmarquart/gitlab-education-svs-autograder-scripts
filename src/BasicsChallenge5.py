from verifiers import *

# A. Sign in to the GitLab instance
# Not checkable.

# B. Create a new Node JS Express project with Auto DevOps
group_name: str = 'Group1'
project_name: str = 'Auto DevOps-test'
project_path: str = f'{group_name}/{project_name}'

if not project_exists_remote(project_path):
    fail_challenge(f"Failed in section B. "
                   f"Did you make a project called '{project_name}' "
                   f"in the group called '{group_name}'?")

file_path: str = '.node-version'
branch_name: str = 'master'
if not file_exists_remote(project_path, file_path, branch_name):
    fail_challenge(f"Failed in section B. "
                   f"Is the '{project_name}' project "
                   f"in the group called '{group_name}' "
                   f"based on the 'NodeJS Express' template?")

branch_name = 'new-feature'
if not branch_exists_remote(project_path, branch_name):
    fail_challenge(f"Failed in section B. "
                   f"Did you create a branch called '{branch_name}' "
                   f"in the project called '{project_name}' "
                   f"in the group '{group_name}'?")


# C. Commit a change to trigger a pipeline run
file_path: str = 'views/index.pug'
text: str = 'p GitLab welcomes you to #{title}'

if not file_contains_text_remote(project_path, file_path, branch_name, text):
    fail_challenge(f"Failed in section C. "
                   f"Did you make a commit that changes the file '{file_path}' "
                   f"to contain the text '{text}', "
                   f"on the branch '{branch_name}' "
                   f"in the project '{project_name}' "
                   f"in the group '{group_name}'?")

commit_message: str = 'Update welcome message in index.pug'

if not commit_exists_with_message_remote(project_path, branch_name, commit_message):
    fail_challenge(f"Failed in section C. "
                   f"Did you make a commit with the message '{commit_message}' "
                   f"on the branch '{branch_name}' "
                   f"in the project '{project_name} "
                   f"in the group '{group_name}'?")

merge_request_title: str = 'Draft: New feature'
if not project_has_merge_request(project_path, merge_request_title):
    fail_challenge(f"Failed in section C. "
                   f"Did you create a merge request called '{merge_request_title}' "
                   f"in the project '{project_name}' "
                   f"in the group '{group_name}'?")


# D. Finish
# Not checkable.
