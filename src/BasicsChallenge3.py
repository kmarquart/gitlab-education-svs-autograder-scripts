from verifiers import *


# A. Sign in to the GitLab instance
# Not checkable.

# B. Create a new project and issue
group_name: str = 'Training Group'
project_name: str = 'Second Project'
project_path: str = f'{group_name}/{project_name}'
if not project_exists_remote(project_path):
    fail_challenge(f"Failed in section B. Did you make a project '{project_name}' in the group '{group_name}'?")

# TODO: the lab asks student to give this project "internal" visibility,
# but that's not possible in the GitLab instance I set up to test this script with.
# Is it possible with Instruqt? If not, change the lab to tell the student to make a project with "private" visibility
visibility: str = 'private'
if not project_has_visibility(project_path, visibility):
    fail_challenge("Failed in section B. "
                   f"Did you give '{visibility}' visibility to the project '{project_name}' "
                   f"in the group '{group_name}'?")

branch: str = 'main'
file_path: str = 'README.md'
if not file_exists_remote(project_path, file_path, branch):
    fail_challenge("Failed in section B. "
                   "Did you check the 'Initialize repository with a README' checkbox "
                   f"when you created '{project_name}'?")

issue_title: str = 'new issue'
if not issue_exists(project_path, issue_title):
    fail_challenge("Failed in section B. "
                   f"Did you make an issue called '{issue_title}' in the project '{project_name}' "
                   f"in the group '{group_name}'? " + CASE_WARNING)

if not issue_assigned(project_path, issue_title, 'gitlab-student'):
    fail_challenge("Failed in section B. "
                   f"Did you assign the issue called '{issue_title}' in the project '{project_name}' to yourself?")

# C. Create a merge request
merge_request_title: str = 'Resolve "new issue"'
merge_request_title_draft: str = 'Draft: Resolve "new issue"'

if not project_has_merge_request(project_path, merge_request_title) and \
        not project_has_merge_request(project_path, merge_request_title_draft):
    fail_challenge("Failed in section C. "
                   f"Did you create a merge request called '{merge_request_title}' or '{merge_request_title_draft}' "
                   f"in the project '{project_name}' in the group '{group_name}'? " + CASE_WARNING)

# D. Edit files on a branch, using GitLab's Web IDE
# Not checkable, since the student edits file on a branch and later merges + deletes the branch.
# Instead, check that these edits show up in `main` down in section F.

# E. Verify changes in a merge request
if not merge_request_has_human_comment(project_path, merge_request_title) and \
        not merge_request_has_human_comment(project_path, merge_request_title_draft):
    fail_challenge("Failed in section E. "
                   f"Did you add a comment to the self-assigned merge request titled '{merge_request_title}' "
                   f"or '{merge_request_title_draft}' in the project '{project_name}' in the group '{group_name}'?")

# F. Merge the branch and close the merge request
if not merge_request_merged(project_path, merge_request_title):
    fail_challenge("Failed in section F. "
                   f"Did you merge the merge request titled '{merge_request_title}' "
                   f"in the project '{project_name}' in the group '{group_name}'?")

contents: str = 'Edit my README.md file'
source_branch: str = '1-new-issue'
target_branch: str = 'main'
if not file_contains_text_remote(project_path, file_path, target_branch, contents):
    fail_challenge("Failed in section D. "
                   f"Did you make a commit to the '{source_branch}' branch in the project '{project_name}' "
                   f"in the group '{group_name}', where that commit edits the file '{file_path}' so it contains "
                   f"the text '{contents}'? "
                   f"And did you merge that branch into the '{target_branch}' branch? " + CASE_WARNING)
