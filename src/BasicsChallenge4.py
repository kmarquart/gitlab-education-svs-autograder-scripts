from verifiers import *

# A. Sign in to the GitLab instance
# Not checkable.

# B. Create a new project and add a CI/CD configuration file
group_name: str = 'Group1'
project_name: str = 'CI Test'
project_path: str = f'{group_name}/{project_name}'

if not project_exists_remote(project_path):
    fail_challenge(f"Failed in section B. Did you make a project '{project_name}' in the group '{group_name}'?")

branch: str = 'main'
file_path: str = '.gitlab-ci.yml'

if not file_exists_remote(project_path, file_path, branch):
    fail_challenge(f"Failed in section B. "
                   f"Did you create a new file '{file_path}' in project '{project_name}' in group '{group_name}'?")

texts = ['echo "Do your build here"',
         'echo "Do a test here"',
         'stages:',
         '- test']

if not file_contains_texts_remote(project_path, file_path, branch, texts):
    fail_challenge(f"Failed in section B. "
                   f"File '{file_path}' in project '{project_name}' in group '{group_name}' "
                   f"doesn't have the expected contents. Did you follow all the instructions in "
                   f"section B exactly?")

# C. Inspect the CI/CD pipeline
# Not checkable.

# D. Finish
# Not checkable.
